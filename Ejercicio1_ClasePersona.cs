using System;

namespace Practica_5_C_
{
   public class Persona
    {
        private string nombre;
        private string apellido;
        private long cedula;
        private int edad;

    class Profesor: Persona
    {
        private float sueldo;

        public void Sueldo()
        {
            Console.Write("Ingrese su sueldo profesor: \n");
            sueldo = float.Parse(Console.ReadLine());
        }
       public void ImprimirSueldo()
       {
           Console.Write("Su sueldo es igual a: RD$"+sueldo+"\n");
       }
       
    }
        public void Mostrar()
        {
            Console.Write("Ingrese su Nombre\n");
            nombre = Console.ReadLine();
            Console.Write("Ingrese su apellido\n");
            apellido = Console.ReadLine();
            Console.Write("Ingrese su cedula\n");
            cedula = long.Parse(Console.ReadLine());
            Console.Write("Ingrese su edad\n");
            edad = int.Parse(Console.ReadLine());
           
        }

        public void Imprimir()
        {
            Console.Write(nombre+" "+apellido+"\n");
            Console.Write("Portador de la Cedula: "+cedula+"\n");
            Console.Write("Edad: "+edad+"\n");
        }
        static void Main(string[] args)
        {
               /* 1. Crear una clase Persona que tenga como atributos el "cedula, nombre, apellido y la edad (definir las propiedades para poder acceder a dichos atributos)". Definir como responsabilidad un método para mostrar ó imprimir. Crear una segunda clase Profesor que herede de la clase Persona. Añadir un atributo sueldo ( y su propiedad) y el método para imprimir su sueldo. Definir un objeto de la clase Persona y llamar a sus métodos y propiedades. También crear un objeto de la clase Profesor y llamar a sus métodos y propiedades. */
               Persona per = new Persona();
               per.Mostrar();
               per.Imprimir();

               Profesor pro = new Profesor();
               Console.Write("\nSolicitud de datos de Profesor (es)\n\n");
               pro.Mostrar();
               pro.Sueldo();
               Console.WriteLine("\nLos datos del profesor son: \n");
               pro.Imprimir();
               pro.ImprimirSueldo();
        }
    }
}
